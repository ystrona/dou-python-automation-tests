from topic_check_page import TopicCheckPage
from selenium.webdriver.support.ui import Select


class AddNewTopicPage(object):
    __title_input_element_id = "txtTitle"
    __category_selector_id = "cbCategory"

    __text_input_id = "txtText"
    __submit_button_element_id = "btnSubmit"

    def add_topic(self, title, category, text):
        self.title_input_element.send_keys(title)
        self.category_selector.select_by_visible_text(category)
        self.text_input.send_keys(text)
#        self.submit_button_element.click()
        return TopicCheckPage(self.driver)

    def __init__(self, driver):
        self.driver = driver
        self.title_input_element = self.driver.find_element_by_id(self.__title_input_element_id)
        self.category_selector = Select(self.driver.find_element_by_id(self.__category_selector_id))
        self.text_input = self.driver.find_element_by_id(self.__text_input_id)
        self.submit_button_element = self.driver.find_element_by_id(self.__submit_button_element_id)