# -*- coding: utf-8 -*-
from add_new_topic_page import AddNewTopicPage


class ForumMainPage(object):
    __add_new_topic_element_xpath = "//*[@class = 'wrap add-content-link']"

    def navigate_to_add_new_topic_page(self):
        add_new_topic_element = self.driver.find_element_by_xpath(self.__add_new_topic_element_xpath)
        add_new_topic_element.click()
        return AddNewTopicPage(self.driver)

    def __init__(self, driver):
        self.driver = driver
