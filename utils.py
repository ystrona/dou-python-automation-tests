from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


def check_element_exists(driver, locator):
    try:
        driver.find_element(*locator)
    except NoSuchElementException:
        return False
    return True


def check_element_exist_by_xpath(driver, xpath):
    try:
        check_element_exists(driver, (By.XPATH, xpath))
    except NoSuchElementException:
        return False
    return True
