from jobs_result_page import JobsResultPage


class JobsPage(object):
    __name = "search"

    def search(self, text):
        self.search_field.send_keys(text)
        self.search_field.submit()
        return JobsResultPage(self.driver)

    def __init__(self, driver):
        self.driver = driver
        self.search_field = self.driver.find_element_by_name(self.__name)
