# -- coding: utf-8 --
import unittest
from selenium import webdriver

from main_page import MainPage
from topic_check_page import TopicCheckPage


class DouSmokeTests(unittest.TestCase):
    __dou_ulr = "https://dou.ua"

    def setUp(self):
        self.driver = webdriver.Chrome()

    def tearDown(self):
        self.driver.close()

    # Dou needs to support registration at least
    # with google account as not all functionality in Dou
    # should be available for un-authorized users

        # Given I have google account, already registered in Dou
        # When I login with this account in dou
        # Then I'm logged in

    @unittest.skip("skip")
    def test_login_with_google(self):
        driver = self.driver

        email = "doutest07@gmail.com"
        password = "doutest071234"

        MainPage(driver, self.__dou_ulr)\
            .login(email, password)\
            .check_user_is_logged_in()

    # Dou must show sponsors list on the home page as
    # this is the main monetization source for Dou

        # Given dou web-site is available
        # When I'm on home page
        # Then I can see sponsors list

    # Note: we'd probably check specific sponsors from DB,
    # but we don't have access to it, so just smoke display test

    @unittest.skip("skip")
    def test_sponsors_block_display(self):
        driver = self.driver

        assert MainPage(driver, self.__dou_ulr)\
            .list_of_sponsors

    # As Dou already has a lot of articles
    # users should be able to search information across the site

        # Given Dou has some articles with <text>
        # When I'm on home page
        # And I search <text> via search input
        # Then I can see results containing <text>

    @unittest.skip("skip")
    def test_search_by_text(self):
        driver = self.driver

        text = "QA"

        assert MainPage(driver, self.__dou_ulr)\
            .search_by_text(text)\
            .check_text_in_search_result(text)

    # Dou has a strong pre-moderation for articles that's why
    # not all users may publish articles. Instead all logged in
    # users may start topics in the Forums page

        # Given I'm a logged in to Dou
        # When navigate to Forums page
        # And I create topic
        # Then my topic is on moderation

    @unittest.skip("Ignoring this test as we don't want to create real topics against production servers")
    def test_add_new_topic_to_forum_for_logged_in_users(self):
        driver = self.driver

        title = "TestTitle"
        category = u'Барахолка'
        text = "test Text test Text test Text test Text"

        email = "doutest07@gmail.com"
        password = "doutest071234"

        MainPage(driver, self.__dou_ulr) \
            .login(email, password) \
            .check_user_is_logged_in()

        MainPage(driver, self.__dou_ulr).navigate_to_forum_main_page()\
            .navigate_to_add_new_topic_page()\
            .add_topic(title, category, text)

        assert TopicCheckPage(driver).check_topic_is_on_moderation()

    # Dou should provide interactive salary widget
    # As salary information is the most popular topic among Dou users

        # Given Dou Database has some salary info for <x>
        # When I navigate to salaries widget
        # And I filter on <x>
        # Then widget shows correct salary info for <x>

    @unittest.skip("skip")
    def test_check_salary(self):
        driver = self.driver

        # Note: it is supposed we have this data from database or something simillar
        period = "may2016"
        city = u'Винница'
        position = "Team lead"
        salary_min = "2100"

        MainPage(driver, self.__dou_ulr).navigate_to_salaries_page()\
            .change_salary_widget(period, city, position)\
            .wait_and_compare_salary_data(salary_min)

    # Dou wants to be the main vacancy site for IT jobs
    # that's why all dou users may search vacancies

        # Given I'm and unauthorized user
        # And dou has some vacancies for <x>
        # When I navigate to vacancies page
        # And search for <x>
        # Then I see relevant vacancies

    @unittest.skip("skip")
    def test_vacancies_search(self):
        driver = self.driver

        search_text = "qa"

        assert MainPage(driver, self.__dou_ulr).navigate_to_job_page()\
            .search(search_text)\
            .vacancies_list


if __name__ == '__main__':
    unittest.main()