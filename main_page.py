# -*- coding: utf-8 -*-

from selenium.webdriver.support.ui import WebDriverWait
from search_result_page import SearchResultPage
from forum_main_page import ForumMainPage
from salaries_page import SalariesPage
from jobs_page import JobsPage
from google_login_page import GoogleLoginPage


class MainPage(object):

    __login_link_id = "login-link"
    __pop_up_window_id = "_loginDialog"
    __login_with_google_link_id = "btnGoogle"
    __email_field_id = 'Email'
    __next_button_id = 'next'
    __password_field_id = 'Passwd'
    __sign_in_button_id = 'signIn'

    __sponsors_block_element_xpath = "//*[@class = 'b-block b-sponsors']"
    __list_of_sponsors_xpath = "//aside//*[@class = 'row']"

    __search_field_element_id = "txtGlobalSearch"
    __search_result_title_list_xpath = "//*[@class = 'g-page']" \
                                       "//*[@class = 'gs-title gsc-table-cell-thumbnail gsc-thumbnail-left']"

    __user_profile_xpath = "//header//a[@class= 'min-profile']"
    __forum_button_element_xpath = '//header//a[contains(. ,"Форум")]'
    __salaries_button_element_xpath = '//header//a[contains(. ,"Зарплаты")]'
    __job_button_element_xpath = '//header//a[contains(. ,"Работа")]'

    def login(self, email, password):
        home_window = self.driver.window_handles[0]
        login_link_element = self.driver.find_element_by_id(self.__login_link_id)
        login_link_element.click()
        login_with_google_link_element = self.driver.find_element_by_id(self.__login_with_google_link_id)
        login_with_google_link_element.click()

        WebDriverWait(self.driver, 10).until(lambda driver: len(driver.window_handles) == 2)

        authorization_window = self.driver.window_handles[1]
        self.driver.switch_to_window(authorization_window)

        GoogleLoginPage(self.driver).login(email, password)

        WebDriverWait(self.driver, 10).until(lambda driver: len(driver.window_handles) == 1)
        self.driver.switch_to_window(home_window)

        return self

    def check_user_is_logged_in(self):
        self.driver.find_element_by_xpath(self.__user_profile_xpath)
        return True

    def search_by_text(self, text):
        search_field_element = self.driver.find_element_by_id(self.__search_field_element_id)
        search_field_element.send_keys(text)
        search_field_element.submit()
        return SearchResultPage(self.driver)

    def navigate_to_forum_main_page(self):
        self.forum_button.click()
        return ForumMainPage(self.driver)

    def navigate_to_salaries_page(self):
        self.salaries_button.click()
        return SalariesPage(self.driver)

    def navigate_to_job_page(self):
        self.job_button.click()
        return JobsPage(self.driver)

    def __init__(self, driver, __dou_url):
        self.driver = driver
        self.driver.get(__dou_url)
        self.list_of_sponsors = self.driver.find_elements_by_xpath(self.__list_of_sponsors_xpath)
        self.salaries_button = self.driver.find_element_by_xpath(self.__salaries_button_element_xpath)
        self.forum_button = self.driver.find_element_by_xpath(self.__forum_button_element_xpath)
        self.job_button = self.driver.find_element_by_xpath(self.__job_button_element_xpath)

