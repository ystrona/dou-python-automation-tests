class JobsResultPage(object):
    __vacancies_list_xpath = "//ul[@class='lt']/li"

    def __init__(self, driver):
        self.driver = driver
        self.vacancies_list = self.driver.find_elements_by_xpath(self.__vacancies_list_xpath)
