from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException


class SearchResultPage(object):
    __search_result_table = "//div[@class = 'gsc-webResult gsc-result']"
    __search_result_title_xpath = "//*[@class = 'g-page']" \
                                  "//*[@class = 'gs-title gsc-table-cell-thumbnail gsc-thumbnail-left']"
    __search_result_body_xpath = "//*[@class = 'g-page']//*[@class = 'gs-bidi-start-align gs-snippet']"

    __no_results_xpath = "//*[@class = 'g-page']//div[@class = 'gs-webResult gs-result gs-no-results-result'"

    # condition to wait the search result is displayed - either no results
    # (in this case WebElement is returned)
    # or search results (in this case a pair of two lists is returned)
    class wait_for_search_result(object):
        def __init__(self, no_results_locator, results_locator, search_result_title_locator, search_result_body_locator):
            self.no_results_locator = no_results_locator
            self.results_locator = results_locator
            self.search_result_title_locator = search_result_title_locator
            self.search_result_body_locator = search_result_body_locator

        @staticmethod
        def get_element(driver, locator):
            try:
                return driver.find_element_by_xpath(locator)
            except NoSuchElementException:
                return None

        def __call__(self, driver):
            no_results_element = self.get_element(driver, self.no_results_locator)
            results_table_element = self.get_element(driver, self.results_locator)
            if no_results_element is not None:
                return no_results_element
            elif results_table_element is not None:
                return (driver.find_elements_by_xpath(self.search_result_title_locator),
                        driver.find_elements_by_xpath(self.search_result_body_locator))
            else:
                return False

    def check_text_in_search_result(self, text):
        for title, body in zip(self.search_result_title_list, self.search_result_body_list):
            if text.lower() not in title.text.lower() and text.lower() not in body.text.lower():
                return False
        return True

    def __init__(self, driver):
        self.driver = driver
        results_elem = WebDriverWait(self.driver, 10).until(
            self.wait_for_search_result(self.__no_results_xpath, self.__search_result_table,
                                        self.__search_result_title_xpath, self.__search_result_body_xpath))

        # search return non-empty result
        if type(results_elem) is tuple:
            (self.search_result_title_list, self.search_result_body_list) = results_elem
        else:
            self.no_results_elem = results_elem
