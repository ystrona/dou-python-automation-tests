# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class SalariesPage(object):
    __period_field_name = "period"
    __city_field_name = "city"
    __position_field_name = "title"
    __language_field_name = "language"
    __salary_min_xpath = "//*[@class = 'salarydec-results-min']/span"

    def change_salary_widget(self, period, city, position):
        self.period_field_name.select_by_value(period)
        self.city_field_element.select_by_visible_text(city)
        self.position_field_element.select_by_visible_text(position)
        return self

    def wait_and_compare_salary_data(self, salary_min):
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.text_to_be_present_in_element((By.XPATH, self.__salary_min_xpath), salary_min))

    def __init__(self, driver):
        self.driver = driver
        self.period_field_name = Select(self.driver.find_element_by_name(self.__period_field_name))
        self.city_field_element = Select(self.driver.find_element_by_name(self.__city_field_name))
        self.position_field_element = Select(self.driver.find_element_by_name(self.__position_field_name))

