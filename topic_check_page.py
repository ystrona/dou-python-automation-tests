# -- coding: utf-8 --
class TopicCheckPage(object):
    __check_text_element_tag_name = "h1"
    __moderation_text = u"топик отправлен на проверку модераторам"

    def check_topic_is_on_moderation(self):
        moderation_status = self.driver.find_element_by_tag_name(self.__check_text_element_tag_name)
        if self.__moderation_text in moderation_status.text:
            return True
        else:
            return False

    def __init__(self, driver):
        self.driver = driver
