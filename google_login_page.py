from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class GoogleLoginPage(object):

    __email_field_id = 'Email'
    __next_button_id = 'next'
    __password_field_id = 'Passwd'
    __sign_in_button_id = 'signIn'

    def login(self, email, password):
        self.driver.find_element_by_id(self.__email_field_id).send_keys(email)
        self.driver.find_element_by_id(self.__next_button_id).click()

        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.ID, self.__password_field_id)))
        self.driver.find_element_by_id(self.__password_field_id).send_keys(password)
        self.driver.find_element_by_id(self.__sign_in_button_id).click()

        return self

    def __init__(self, driver):
        self.driver = driver
